package com.mcnc.bizmob.plugin.push.fcm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.mcnc.bizmob.plugin.push.util.PushUtil;
import com.ppcb.mbanking.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class BMFirebaseMessagingService extends FirebaseMessagingService {
    /**
     * PowerManager 객체
     */
    private PowerManager pm = null;

    /**
     * PowerManager가 보유한 wake / lock 기능을 수행하는 객체
     */
    private PowerManager.WakeLock wl = null;

    private static final String TAG = BMFirebaseMessagingService.class.getSimpleName();

    /**
     * push 작업과 관련된 수행 결과를 올려 줄 callback function 명.
     */
    private static String resultPushData = "";

    /**
     * WakeRock 해제 기능을 수행할 Runnalbe 객체
     */
    private Runnable run = new Runnable() {
        @Override
        public void run() {
            if (wl != null) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                wl.release();
            }
        }
    };

    private String channelId = "";

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("pushPref", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("registrationId", token);
        editor.commit();
    }

    // 메시지 수신
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "FCM onMessageReceived");
        Log.d(TAG, "FCM, From : " + remoteMessage.getFrom());

        sendNotification(remoteMessage);
    }

    @SuppressLint("InvalidWakeLockTag")
    private void sendNotification(RemoteMessage remoteMessage) {
        Log.d(TAG, "FCM sendNotification");
        Context context = getApplicationContext();

        String from = remoteMessage.getFrom();
        if (from != null && from.equals("google.com/iid")) {
            return;
        }

        String title = "";
        String body = "";
        RemoteMessage.Notification remoteMessageNotification =  remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();

        if(remoteMessageNotification != null) {
            title = remoteMessage.getNotification().getTitle();
            body = remoteMessage.getNotification().getBody();

            String clickAction = remoteMessage.getNotification().getClickAction();

            if (clickAction == null) {
                clickAction = "";
            }

            Log.d(TAG, " onMessageReceived () ------------ CLICK ACTION : " + clickAction);
        }else {
            title = getString(data, "title", "알림");
            body = getString(data, "message", "");
        }

        String count = getString(data, "badge", "0");
        String hasImage = getString(data, "hasBin", "");
        String messageId = getString(data, "messageId", "");

        Log.d(TAG, " onMessageReceived () ------------ COME TITLE: " + title);
        Log.d(TAG, " onMessageReceived () ------------ COME BODY: " + body);
        Log.d(TAG, " onMessageReceived () ------------ MESSAGE ID : " + messageId);

        pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        wl.acquire();

        String type = "";
        String url = "";

        if (hasImage != null && hasImage.equals("true")) {
            type = "bigPicture";

            String strUrl = PushUtil.getPushUrl(context);

            int len = strUrl.length();
            int lastIndex = strUrl.lastIndexOf("/");

            if (len != lastIndex + 1) {
                strUrl += "/";
            }

            url = strUrl + "messages/" + messageId + "/binaries/0/download/pushimage.jpg";
        }

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = null;

        // notiId
        int notiID = (int) System.currentTimeMillis();
        Intent in = new Intent(Intent.ACTION_MAIN);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.addCategory(Intent.CATEGORY_LAUNCHER);
        in.setComponent(new ComponentName(context, FcmControlActivity.class));
        in.putExtra("push", body);
        in.putExtra("push_noti_id", notiID);
        in.putExtra("message_id", messageId);
        in.putExtra("fromPush", "push");

        in.putExtra("badge", count);
        in.putExtra("message", body);
        in.putExtra("hasBin", hasImage);

        in.putExtra("fromPush", "bizPush");

        int badgeCount = 0;

        try {
            badgeCount = Integer.parseInt(count);
        } catch (Exception e) {
            badgeCount = 0;
        }

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("pushPref", Activity.MODE_PRIVATE);
        channelId = preferences.getString("channelId", "");

        if (type.equals("bigText")) {
            notification = bigTextStyle(context, title, body, notiID, in, badgeCount);
        } else if (type.equals("bigPicture")) {
            notification = bigImageStyle(context, title, body, url, notiID, in, badgeCount);
        } else {
            notification = normalStyle(context, title, body, notiID, in, badgeCount);
        }

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        // 전체 제거 고려
        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
            notification.defaults |= Notification.DEFAULT_SOUND;
        } else {
            // 진동 효과 구성
            long[] vibrate = {1000, 1000, 1000, 1000, 1000};
            notification.vibrate = vibrate;
        }
        nm.notify("TAG", 003123, notification);
        run.run();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Intent badgeIntent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
            badgeIntent.putExtra("badge_count", badgeCount);
            badgeIntent.putExtra("badge_count_package_name", context.getPackageName());
            badgeIntent.putExtra("badge_count_class_name", PushUtil.getLauncherClassName(context));
            context.sendBroadcast(badgeIntent);
        }
    }

    /**
     * 전달 받은 데이터를 가공하여 normal 스타일의 notification 을 띄우는 메소드.<br>
     * <p>
     * <pre>
     * 수정이력 <br>
     * **********************************************************************************************************************************<br>
     *  수정일                               이름                               변경내용<br>
     * **********************************************************************************************************************************<br>
     *  2016-09-22                           박재민                             최초 작성<br>
     *
     * </pre>
     *
     * @param title   notification 제목 문자열.
     * @param message notification 내용 문자열.
     * @param notiID  notification 고유 Id 값.
     * @param in      이동할 화면 정보를 갖고 있는 intent 객체.
     * @return 생성된  normal 스타일의 Notification 객체.
     */
    private Notification normalStyle(Context context, String title, String message, int notiID, Intent in, int number) {
        if (title == null || (title != null && title.equals(""))) {
            title = "알림";
        }

        int notiIconId = R.drawable.noti_icon;

        Notification notification;

        PendingIntent pendingIntent = PendingIntent.getActivity(context, notiID, in, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(notiIconId)
                .setTicker("새 메시지 도착")
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true);

        builder.setContentIntent(pendingIntent);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setNumber(number);

        notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL; //선택시 자동삭제

        return notification;
    }

    /**
     * 전달 받은 데이터를 가공하여 big text 스타일의 notification 을 띄우는 메소드.<br>
     * <p>
     * <pre>
     * 수정이력 <br>
     * **********************************************************************************************************************************<br>
     *  수정일                               이름                               변경내용<br>
     * **********************************************************************************************************************************<br>
     *  2016-09-22                           박재민                             최초 작성<br>
     *
     * </pre>
     *
     * @param title   notification 제목 문자열.
     * @param message notification 내용 문자열.
     * @param notiID  notification 고유 Id 값.
     * @param in      이동할 화면 정보를 갖고 있는 intent 객체.
     * @return 생성된  normal 스타일의 Notification 객체.
     */
    private Notification bigTextStyle(Context context, String title, String message, int notiID, Intent in, int number) {
        if (title == null || (title != null && title.equals(""))) {
            title = "알림";
        }

        int notiIconId = R.drawable.noti_icon;

        Notification notification;

        PendingIntent pendingIntent = PendingIntent.getActivity(context, notiID, in, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(notiIconId)
                .setTicker("새 메시지 도착")
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true);

        // big text style
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
        style.setBigContentTitle(title);
        style.bigText(message);

        builder.setStyle(style);
        builder.setContentIntent(pendingIntent);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setNumber(number);

        notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL; //선택시 자동삭제

        return notification;
    }

    /**
     * 전달 받은 데이터를 가공하여 big image 스타일의 notification 을 띄우는 메소드.<br>
     * <p>
     * <pre>
     * 수정이력 <br>
     * **********************************************************************************************************************************<br>
     *  수정일                               이름                               변경내용<br>
     * **********************************************************************************************************************************<br>
     *  2016-09-22                           박재민                             최초 작성<br>
     *
     * </pre>
     *
     * @param title   notification 제목 문자열.
     * @param message notification 내용 문자열.
     * @param url     이미지 url.
     * @param notiID  notification 고유 Id 값.
     * @param in      이동할 화면 정보를 갖고 있는 intent 객체.
     * @return 생성된  normal 스타일의 Notification 객체.
     */
    private Notification bigImageStyle(Context context, String title, String message, String url, int notiID, Intent in, int number) {
        if (title == null || (title != null && title.equals(""))) {
            title = "알림";
        }

        int notiIconId = R.drawable.noti_icon;

        Notification notification;

        PendingIntent pendingIntent = PendingIntent.getActivity(context, notiID, in, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(notiIconId)
                .setTicker("새 메시지 도착")
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true);

        Bitmap bitmap = null;
        try {

            bitmap = drawableFromUrl(url).getBitmap();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bitmap != null) {
            // big image style
            NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
            style.setBigContentTitle(title);
            style.setSummaryText(message);

            style.bigPicture(bitmap);


            builder.setStyle(style);
            Log.d("notiIconId", "bigImageStyle bitmap != null");
        } else {

            Log.d("notiIconId", "bigImageStyle bitmap == null");
        }
        builder.setContentIntent(pendingIntent);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setNumber(number);

        notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL; //선택시 자동삭제

        return notification;
    }

    /**
     * 이미지 url 로 부터 BitmapDrawable 객체를 반환해주는 메소드.<br>
     * <p>
     * <pre>
     * 수정이력 <br>
     * **********************************************************************************************************************************<br>
     *  수정일                               이름                               변경내용<br>
     * **********************************************************************************************************************************<br>
     *  2016-09-22                           박재민                             최초 작성<br>
     *
     * </pre>
     *
     * @param url 이미지 url.
     * @return 이미지 url로 부터 생성된 BitmapDrawable 객체
     */
    private BitmapDrawable drawableFromUrl(String url) throws IOException {
        Bitmap bitmap = null;
        HttpURLConnection connection = null;
        InputStream input = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.connect();
            input = connection.getInputStream();

            Log.d("PUSH", "connection.getContentLength() : " + connection.getContentLength());
            Log.d("PUSH", "connection.getResponseCode() : " + connection.getResponseCode());
            Log.d("PUSH", "connection.getResponseMessage() : " + connection.getResponseMessage());

            bitmap = BitmapFactory.decodeStream(input);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (bitmap == null) {
                try {
                    connection = (HttpURLConnection) new URL(url).openConnection();
                    connection.connect();
                    input = connection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(input);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (input != null) {
                        try {
                            input.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if (connection != null) {
                        try {
                            connection.disconnect();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                Log.d("PUSH", "connection.getContentLength() : " + connection.getContentLength());
                Log.d("PUSH", "connection.getResponseCode() : " + connection.getResponseCode());
                Log.d("PUSH", "connection.getResponseMessage() : " + connection.getResponseMessage());


                if (bitmap == null) {
                    Log.d("PUSH", "The bitmap is still null. So the image will be removed from the notification.");
                }
            }
        }
        return new BitmapDrawable(bitmap);
    }

    private String getString(Map<String, String> data, String key, String defaultValue){
        String result = data.get(key);

        if(TextUtils.isEmpty(result)){
            result = defaultValue;
        }

        return result;
    }
}
