package com.mcnc.bizmob.plugin.push.fcm;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.mcnc.bizmob.plugin.push.util.PushUtil;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

/**
 * 01.클래스 설명 : Fcm Notification click_action에 정의 된 Activity. <br>
 * 02.제품구분 : bizMOB 3.0 Android Container <br>
 * 03.기능(콤퍼넌트) 명 : FCM notification 수신 후 화면 이동  <br>
 * 04.관련 API/화면/서비스 : Logger, BMCFragment, BMCInit <br>
 * 05.수정이력  <br>
 * <pre>
 * **********************************************************************************************************************************<br>
 *  수정일                                          이름                          변경 내용<br>
 * **********************************************************************************************************************************<br>
 *  2018-08-03                                     박재민                         최초 작성<br>
 * **********************************************************************************************************************************<br>
 * </pre>
 *
 * @author 박재민
 * @version 1.0
 */
public class FcmControlActivity extends Activity {

    /**
     * class 명
     */
    private String TAG = this.toString();

    /**
     * callback 줄 function 명
     */
    private String callback = null;

    /**
     * 특정 정보를 담고 담고 있는 문자열
     */
    private String data = null;


    /**
     * 외부로 부터 전달 받은 호출에 반응하여, 특정화면을 호출하며, Push 호출인 경우 push event 발생을 웹에게 알려주는 메소드를 호출 하는 메소드.<br>
     * <p>
     * <pre>
     * 수정이력 <br>
     * **********************************************************************************************************************************<br>
     *  수정일                               이름                               변경내용<br>
     * **********************************************************************************************************************************<br>
     *  2016-09-22                           박재민                             최초 작성<br>
     *
     * </pre>
     *
     * @param savedInstanceState Bundle 객체. 로직과 상관 없음.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        final Intent intent = getIntent();

        sendBroadCast();

        callback = "onPush";
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Bundle bundle = intent.getExtras();

                    String badge = bundle.getString("badge");
                    String message = bundle.getString("message");
                    String hasBin = bundle.getString("hasBin");
                    String meesageId = bundle.getString("messageId");

                    JSONObject pushObj = new JSONObject();
                    pushObj.put("badge", badge);
                    pushObj.put("message", message);
                    pushObj.put("hasBin", hasBin);
                    pushObj.put("messageId", meesageId);

                    Log.d(TAG, "intent data : " + pushObj.toString());

                    data = pushObj.toString();
                    startActivity(callback, data);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();


        this.finish();
    }

    private void sendBroadCast() {
        Intent intent = new Intent("notification-click");
        // You can also include some extra data.
        // intent.putExtra("message", "This is my message!");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * 현재 앱의 화면 개수를 판단하여, 앱이 실행 중 인지 여부를 판단하며, 앱실행 또는 현재 화면에 event noti를 주는 메소드. <br>
     * <p>
     * <pre>
     * 수정이력 <br>
     * **********************************************************************************************************************************<br>
     *  수정일                               이름                               변경내용<br>
     * **********************************************************************************************************************************<br>
     *  2016-09-22                           박재민                             최초 작성<br>
     *
     * </pre>
     *
     * @param callback 클라이언트의 callback function 명.
     * @param data     callback 줄 data 값.
     */
    private void startActivity(final String callback, final String data) {
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfoList = activityManager.getRunningTasks(10);
        Iterator<ActivityManager.RunningTaskInfo> iterator = runningTaskInfoList.iterator();

        boolean checkRunning = false;

        String launcherActivity = PushUtil.getLauncherClassName(this);

        while(iterator.hasNext()){
            ActivityManager.RunningTaskInfo runningTaskInfo = iterator.next();
            if(runningTaskInfo.topActivity.getClassName().equals(launcherActivity)){
                checkRunning = true;
                break;
            }
        }

        if(checkRunning){

        } else {
            try {
                Class launcherClass = Class.forName(launcherActivity);
                Intent intent = new Intent(this, launcherClass);

                if (callback != null && callback.length() > 0) {
                    intent.putExtra("external_callback", callback);
                }

                if (data != null) {
                    intent.putExtra("external_data", data);
                }

                startActivity(intent);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
