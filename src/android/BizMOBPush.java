package com.mcnc.bizmob.plugin.push;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.mcnc.bizmob.plugin.push.util.PushUtil;

public class BizMOBPush extends CordovaPlugin {
    private String TAG = this.toString();
    private CallbackContext mCallbackContext;

    private static final String ACTION_GET_PUSHKEY = "GET_PUSHKEY";
    private static final String ACTION_PUSH_REGISTRATION = "PUSH_REGISTRATION";
    private static final String ACTION_PUSH_ALARM_SETTING_INFO = "PUSH_ALARM_SETTING_INFO";
    private static final String ACTION_PUSH_UPDATE_ALARM_SETTING = "PUSH_UPDATE_ALARM_SETTING";
    private static final String ACTION_PUSH_GET_MESSAGES = "PUSH_GET_MESSAGES";
    private static final String ACTION_PUSH_MARK_AS_READ = "PUSH_MARK_AS_READ";
    private static final String ACTION_PUSH_GET_UNREAD_PUSH_MESSAGE_COUNT = "PUSH_GET_UNREAD_PUSH_MESSAGE_COUNT";
    private static final String ACTION_SET_BADGE_COUNT = "SET_BADGE_COUNT";
    private static final String ACTION_SEND_PUSH_MESSAGE = "SEND_PUSH_MESSAGE";
    private static final String ACTION_CHECK_PUSH_RECEIVED = "CHECK_PUSH_RECEIVED";
    private static final String ACTION_GET_PUSH_MESSAGE_TYPE_LIST = "GET_PUSH_MESSAGE_TYPE_LIST";
    private static final String ACTION_GET_PUSH_AGREEMENT_INFO = "GET_PUSH_AGREEMENT_INFO";
    private static final String ACTION_UPDATE_PUSH_AGREEMENT_INFO = "UPDATE_PUSH_AGREEMENT_INFO";
    private static final String ACTION_ON_CLICK = "ON_CLICK";

    private static final String PARAM_HEADER = "header";
    private static final String PARAM_BODY = "body";

    private JSONObject resultData = null;
    private JSONObject resultHeader = null;
    private JSONObject resultBody = null;

    private JSONObject param = null;

    private JSONObject header = null;
    private JSONObject body = null;

    private String type = "";
    private String url = "";

    public static long[] mVibrationPattern = new long[]{100, 200, 100, 200};

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        mCallbackContext = callbackContext;

        param = args.getJSONObject(0);

        resultData = new JSONObject();
        resultHeader = new JSONObject();
        resultBody = new JSONObject();

        resultData.put(PARAM_HEADER, resultHeader);
        resultData.put(PARAM_BODY, resultBody);

        if (param.has(PARAM_HEADER)) {
            header = param.getJSONObject(PARAM_HEADER);
        }
        if (param.has(PARAM_BODY)) {
            body = param.getJSONObject(PARAM_BODY);
        }

        if (action.equals(ACTION_GET_PUSHKEY)) {
            getPushKey();
        } else if (action.equals(ACTION_PUSH_REGISTRATION)) {
            pushRegistration();
        } else if (action.equals(ACTION_ON_CLICK)) {
            handleClickNotification();
//        } else if (action.equals(ACTION_PUSH_ALARM_SETTING_INFO)) {
//            pushAlarmSettingInfo();
//        } else if (action.equals(ACTION_PUSH_UPDATE_ALARM_SETTING)) {
//            pushUpdateAlarmSetting();
//        } else if (action.equals(ACTION_PUSH_GET_MESSAGES)) {
//            pushGetMessages();
//        } else if (action.equals(ACTION_PUSH_MARK_AS_READ)) {
//            pushMarkAsRead();
//        } else if (action.equals(ACTION_PUSH_GET_UNREAD_PUSH_MESSAGE_COUNT)) {
//            pushGetUnreadMessageCount();
//        } else if (action.equals(ACTION_SET_BADGE_COUNT)) {
//            setBadgeCount();
//        } else if (action.equals(ACTION_SEND_PUSH_MESSAGE)) {
//            sendMessages();
//        } else if (action.equals(ACTION_CHECK_PUSH_RECEIVED)) {
//            pushReceivedCheck();
//        } else if (action.equals(ACTION_GET_PUSH_MESSAGE_TYPE_LIST)) {
//            getPushMessageTypeList();
//        } else if (action.equals(ACTION_GET_PUSH_AGREEMENT_INFO)) {
//            getPushAgreementInfo();
//        } else if (action.equals(ACTION_UPDATE_PUSH_AGREEMENT_INFO)) {
//            updatePushAgreementInfo();
        } else {
            resultHeader.put("result", false);
            resultHeader.put("errorCode", "NE0007");
            resultHeader.put("errorText", "INVALID_ACTION");
        }

        sendCallback(resultData);
        return true;
    }

    /**
     * PushKey를 전달해주는 메서드
     */
    private void getPushKey() throws JSONException {
        SharedPreferences preferences = cordova.getActivity().getSharedPreferences("pushPref", Activity.MODE_PRIVATE);
        String fcmToken = preferences.getString("registrationId", "");

        if (!TextUtils.isEmpty(fcmToken)) {
            resultHeader.put("result", true);
            resultHeader.put("errorCode", "");
            resultHeader.put("errorText", "");

            resultBody.put("pushKey", fcmToken);
        } else {
            resultHeader.put("result", false);
            resultHeader.put("errorCode", "NE0009");
            resultHeader.put("errorText", "Pushkey not found");
        }
    }

    /**
     * 디바이스를 bizPush 수신이 가능하도록 등록 하는 메소드를 호출 하는 메소드.<br>
     */
    private void pushRegistration() {
        NotificationManager nm = (NotificationManager) cordova.getActivity().getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        settingChannel(nm);

        try {
            if(body.has("pushUrl")){
                String url = body.getString("pushUrl");

                if(!TextUtils.isEmpty(url)){
                    if(url.lastIndexOf("/") == url.length() - 1){
                        url += "v150/push";
                    } else {
                        url += "/v150/push";
                    }

                    SharedPreferences preferences = cordova.getActivity().getSharedPreferences("pushPref", Activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putString("pushUrl", url);
                    editor.commit();
                }
            }

            String pushKey = body.has("pushKey") ? body.getString("pushKey") : "";
            String userID = body.has("userId") ? body.getString("userId") : "";
            String appName = body.has("appName") ? body.getString("appName") : "";

            int major = 0;
            int minor = 0;
            int build = 0;

            build = cordova.getActivity().getPackageManager().getPackageInfo(cordova.getActivity().getPackageName(), 0).versionCode;
            String appVerString = cordova.getActivity().getPackageManager().getPackageInfo(cordova.getActivity().getPackageName(), 0).versionName;
            java.util.StringTokenizer strToken = new java.util.StringTokenizer(appVerString, ".");
            if (strToken.hasMoreTokens()) {
                String token = strToken.nextToken();
                major = Integer.parseInt(token.trim());
            }
            if (strToken.hasMoreTokens()) {
                String token = strToken.nextToken();
                minor = Integer.parseInt(token.trim());
            }
            // Version String 우선으로 변경.
            if (strToken.hasMoreTokens()) {
                String token = strToken.nextToken();
                build = Integer.parseInt(token.trim());
            } else {
                build = cordova.getActivity().getPackageManager().getPackageInfo(cordova.getActivity().getPackageName(), 0).versionCode;
            }

            String appVersion = major + "." + minor + "." + build;
            String deviceOsType = "ANDROID";
            String pushProviderType = "FCM";

            String deviceOsVersion = Build.VERSION.RELEASE;
            String[] arr = deviceOsVersion.split("\\.");

            if (arr.length == 1) {
                deviceOsVersion += ".0";
            } else {
                deviceOsVersion = arr[0] + "." + arr[1];
            }

            String deviceId = Settings.Secure.getString(cordova.getActivity().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            String deviceType = isTablet(cordova.getActivity()) ? "tablet" : "phone";
            String deviceModel = Build.MODEL;

            String carrierCode = "00000";

            bizPushRegistDeviceInterface(cordova.getActivity(), pushKey, userID, appName, appVersion, deviceOsType, pushProviderType, deviceOsVersion, deviceId, deviceType, deviceModel, carrierCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void settingChannel(NotificationManager nm){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "push_channel";
            String channelName = "푸시 알림";
            String channelDescription = "푸시 알림 채널";

            try {
                NotificationChannel notificationChannel = null;

                if (body.has("channelId")) {
                    channelId = !TextUtils.isEmpty(body.getString("channelId")) ? body.getString("channelId") : channelId;
                }
                if (body.has("channelName")) {
                    channelName = !TextUtils.isEmpty(body.getString("channelName")) ? body.getString("channelName") : channelName;
                }
                if (body.has("channelDescription")) {
                    channelDescription = !TextUtils.isEmpty(body.getString("channelDescription")) ? body.getString("channelDescription") : channelDescription;
                }

                SharedPreferences preferences = cordova.getActivity().getSharedPreferences("pushPref", Activity.MODE_PRIVATE);
                String savedChannelId = preferences.getString("channelId", "");

                SharedPreferences.Editor editor = preferences.edit();

                // 저장된 채널 아이디가 없는 경우, 새로 저장을 한다.
                if (TextUtils.isEmpty(savedChannelId)) {
                    editor.putString("channelId", channelId);
                    editor.commit();

                    notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
                } else {
                    // 저장된 채널 아이디와 넘겨 받은 채널 아이디가 다른 경우 기존 채널을 삭제하고, 넘겨받은 채널 아이디를 저장한다.
                    if (!savedChannelId.equals(channelId)) {
                        nm.deleteNotificationChannel(savedChannelId);

                        editor.putString("channelId", channelId);
                        editor.commit();

                        notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
                    } else {
                        notificationChannel = nm.getNotificationChannel(channelId);
                    }
                }

                if (body.has("notiImportance")) {
                    switch (body.getString("notiImportance")) {
                        case "default":
                            notificationChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
                            break;
                        case "high":
                            notificationChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
                            break;
                        default:
                            notificationChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
                            break;
                    }
                }

                notificationChannel.setDescription(channelDescription);
                notificationChannel.setShowBadge(true);
                notificationChannel.enableLights(true);
                notificationChannel.enableVibration(true);
                notificationChannel.setLightColor(Color.GREEN);
                notificationChannel.setVibrationPattern(mVibrationPattern);
                notificationChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), null);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

                nm.createNotificationChannel(notificationChannel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

//    /**
//     * 설정된 푸시 알람을 갖고 오는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject pushAlarmSettingInfo() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String pushKey = param.getString("push_key");
//        String userId = param.getString("user_id");
//
//        try {
//            result = alarmSettingInfo(pushKey, userId);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//
//    /**
//     * 푸시 알람을 설정 하는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject pushUpdateAlarmSetting() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String pushKey = param.getString("push_key");
//        String userId = param.getString("user_id");
//        boolean enabled = param.getBoolean("enabled");
//
//        try {
//            result = updateAlarmSetting(pushKey, userId, enabled);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//        return result;
//    }
//
//    /**
//     * Push 메시지를 수신 여부를 체크하는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject pushReceivedCheck() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String messageId = param.getString("message_id");
//        String userId = param.getString("user_id");
//        //수신확인
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//        String date = sdf.format(Calendar.getInstance().getTime());
//
//        try {
//            result = receivedCheck(messageId, userId, "CHECKED", date, "true");
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//    /**
//     * 푸시 메세지 목록을 가져오는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject pushGetMessages() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        int pageIndex = param.getInt("page_index");
//        int itemCount = param.getInt("item_count");
//        String userId = param.getString("user_id");
//        String appName = param.getString("app_name");
//
//        try {
//            result = getMessages(pageIndex, itemCount, userId, appName);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//
//    /**
//     * Push 메시지 읽음 처리를 하는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject pushMarkAsRead() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String trxDay = param.getString("trx_day");
//        String userId = param.getString("user_id");
//        String trxId = param.getString("trx_id");
//        boolean read = param.getBoolean("read");
//
//        try {
//            result = markAsRead(trxDay, userId, trxId, read);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//    /**
//     * 읽지 않은 푸시 메세지 개수를 가져오는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject pushGetUnreadMessageCount() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String appName = param.getString("app_name");
//        String userId = param.getString("user_id");
//
//        try {
//            result = getUnreadPushMessage(userId, appName);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//    /**
//     * 뱃지 카운트를 설정하는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject setBadgeCount() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        int badgeCount = param.getInt("badge_count");
//
//        Intent badgeIntent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
//        badgeIntent.putExtra("badge_count", badgeCount);
//        badgeIntent.putExtra("badge_count_package_name", cordova.getActivity().getPackageName());
//        badgeIntent.putExtra("badge_count_class_name", PushUtil.getLauncherClassName(cordova.getActivity()));
//        cordova.getActivity().sendBroadcast(badgeIntent);
//
//        result.put("result", true);
//
//        return result;
//    }
//
//    /**
//     * 푸시 메세지를 전송 하는 메소드를 호출 하는 메소드.<br>
//     */
//    private JSONObject sendMessages() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String trxType = param.getString("trx_type");
//        String scheduleDate = param.getString("schedule_date");
//        String appName = param.getString("app_name");
//        JSONArray toUsers = param.getJSONArray("to_users");
//        JSONArray toGroups = param.getJSONArray("to_groups");
//        boolean toAll = param.getBoolean("to_all");
//        String fromUser = param.getString("from_user");
//        String messageSubject = param.getString("message_subject");
//        String messageCategory = param.getString("message_category");
//        String messageContent = param.getString("message_content");
//        JSONObject messagePayload = param.getJSONObject("message_payload");
//
//        try {
//            result = sendMessages(trxType, scheduleDate, appName, toUsers, toGroups, toAll, fromUser, messageSubject, messageCategory, messageContent, messagePayload);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//    /**
//     * 푸시 메세지의 type list를 받아오는 메소드를 호출 하는 메소드.<br>
//     */
//    public JSONObject getPushMessageTypeList() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        boolean agreeFlag = param.getBoolean("agreeFlag");
//        boolean deletedFlag = param.getBoolean("deletedFlag");
//        JSONArray messageTypeIds = param.getJSONArray("messageTypeIds");
//
//        try {
//            result = getMessageTypeList(agreeFlag, deletedFlag, messageTypeIds);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//    /**
//     * 푸시 수신 동의 정보를 받아오는 메소드를 호출 하는 메소드.<br>
//     */
//    public JSONObject getPushAgreementInfo() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String userId = param.getString("userId");
//        String appName = param.getString("appName");
//        JSONArray messageTypeIds = param.getJSONArray("messageTypeIds");
//
//        try {
//            result = getAgreementInfo(userId, appName, messageTypeIds);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }
//
//    /**
//     * 푸시 수신 동의 정보를 업데이트 하는 메소드를 호출 하는 메소드.<br>
//     */
//    public JSONObject updatePushAgreementInfo() throws JSONException {
//        JSONObject result = new JSONObject();
//
//        String userId = param.getString("userId");
//        String messageTypeId = param.getString("messageTypeId");
//        boolean agreeFlag = param.getBoolean("agreeFlag");
//        String appName = param.getString("appName");
//
//
//        try {
//            result = updateAgreementInfo(userId, messageTypeId, agreeFlag, appName);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.put("result", false);
//        }
//
//        return result;
//    }

    /**
     * DTO(RDataDTO object)를 JSONObject 형태로 반환해 주는 메소드.<br>
     */
    private JSONObject toRDataJSONObject(RDataDTO dto) throws JSONException {
        JSONObject rDataObj = new JSONObject();
        if (dto != null && dto.getKeyValueData() != null) {
            JSONArray arrayObj = new JSONArray();
            for (KeyValueDataDTO keyValueDataDTO : dto.getKeyValueData()) {
                JSONObject keyValueDataObj = new JSONObject();
                keyValueDataObj.put("Key", keyValueDataDTO.getKey());
                keyValueDataObj.put("Value", keyValueDataDTO.getValue());
                arrayObj.put(keyValueDataObj);
            }
            rDataObj.put("KeyValueData", arrayObj);
        } else {
            rDataObj.put("KeyValueData", null);
        }
        return rDataObj;
    }

    /**
     * bizPush Server에 디바이스를 등록해주는 메소드.<br>
     */
    private void bizPushRegistDeviceInterface(Context context, String pushKey, String userId, String appName,
                                                    String appVersion, String deviceOsType, String pushProviderType, String deviceOsVersion,
                                                    String deviceUid, String deviceType, String deviceModel, String carrierCode) throws Exception {

        SharedPreferences preferences = context.getSharedPreferences("pushPref", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        byte[] postData = null;

        JSONObject RData = new JSONObject();

        RData.put("pushKey", pushKey);
        RData.put("userId", userId);
        RData.put("appName", appName);
        RData.put("appVersion", appVersion);
        RData.put("deviceOsType", deviceOsType);
        RData.put("pushProviderType", pushProviderType);
        RData.put("deviceOsVersion", deviceOsVersion);
        RData.put("deviceUid", deviceUid);
        RData.put("deviceType", deviceType);
        RData.put("deviceModel", deviceModel);
        RData.put("carrierCode", carrierCode);

        postData = RData.toString().getBytes();

//        JSONObject responseJSON = new JSONObject();

        if (pushKey == null || userId == null || appName == null || appVersion == null || deviceOsType == null ||
                pushProviderType == null || deviceOsVersion == null || deviceUid == null) {
            resultHeader.put("result", false);
            resultHeader.put("errorCode", "NE0009");
            resultHeader.put("errorText", "JSON_EXCEPTION");
        } else {
            pushConnect("regist", postData);

//            if (responseJSON.getBoolean("result")) {
//                editor.putString("push_type", "biz_push");
//                editor.commit();
//            }
        }
    }

//    /**
//     * Push 메시지를 수신 여부를 체크하는 메소드.<br>
//     */
//    private JSONObject receivedCheck(String messageID, String userID, String checkType, String time, String msgRequest) throws Exception {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("userId", userID);
//        RData.put("messageId", messageID);
//
//        if (checkType.equals("RECEIVED")) {
//            RData.put("arrived", true);
//            RData.put("checked", false);
//        } else {
//            RData.put("arrived", false);
//            RData.put("checked", true);
//        }
//
//        RData.put("messageRequest", msgRequest);
//        RData.put("checkedDate", time);
//        RData.put("arrivedDate", time);
//
//        postData = RData.toString().getBytes();
//
//        return pushConnect("check", postData);
//    }
//
//
//    /**
//     * 읽지 않은 푸시 메세지 개수를 가져오는 메소드.<br>
//     */
//    private JSONObject getUnreadPushMessage(String userId, String appName) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("userId", userId);
//        RData.put("appName", appName);
//
//        postData = RData.toString().getBytes();
//
//        if (userId == null || appName == null) {
//
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//
//        } else {
//            return pushConnect("getUnreadPushMessage", postData);
//        }
//
//    }
//
//    /**
//     * 설정된 푸시 알람을 갖고 오는 메소드.<br>
//     */
//    private JSONObject alarmSettingInfo(String pushKey, String userId) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("pushKey", pushKey);
//        RData.put("userId", userId);
//
//        postData = RData.toString().getBytes();
//
//        if (pushKey == null || userId == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//
//        } else {
//            return pushConnect("alarmSettingInfo", postData);
//        }
//    }
//
//    /**
//     * 푸시 알람을 설정 하는 메소드.<br>
//     */
//    private JSONObject updateAlarmSetting(String pushKey, String userId, boolean enabled) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("pushKey", pushKey);
//        RData.put("userId", userId);
//        RData.put("enabled", enabled);
//
//        postData = RData.toString().getBytes();
//
//        if (pushKey == null || userId == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//        } else {
//            return pushConnect("updateAlarmSetting", postData);
//        }
//    }
//
//    /**
//     * Push 메시지 읽음 처리를 하는 메소드.<br>
//     */
//    private JSONObject markAsRead(String trxDay, String userId, String trxId, boolean read) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("trxDay", trxDay);
//        RData.put("trxId", trxId);
//        RData.put("userId", userId);
//        RData.put("read", read);
//
//        postData = RData.toString().getBytes();
//
//        if (trxDay == null || trxId == null || userId == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//        } else {
//            return pushConnect("markAsRead", postData);
//        }
//    }
//
//    /**
//     * 푸시 메세지 목록을 가져오는 메소드.<br>
//     */
//    private JSONObject getMessages(int pageIndex, int itemCount, String userId, String appName) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("pageIndex", pageIndex);
//        RData.put("itemCount", itemCount);
//        RData.put("appName", appName);
//        RData.put("userId", userId);
//
//        postData = RData.toString().getBytes();
//
//        if (pageIndex == 0 || itemCount == 0 || appName == null || userId == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//        } else {
//            return pushConnect("getMessages", postData);
//        }
//    }
//
//    /**
//     * 푸시 메세지 전송하는 메소드.<br>
//     */
//    private JSONObject sendMessages(String trxType, String scheduleDate, String appName,
//                                    JSONArray toUsers, JSONArray toGroups, boolean toAll, String fromUser, String messageSubject,
//                                    String messageCategory, String messageContent, JSONObject messagePayload) throws JSONException {
//
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("trxType", trxType);
//        RData.put("scheduleDate", scheduleDate);
//        RData.put("appName", appName);
//        RData.put("toUsers", toUsers);
//        RData.put("toGroups", toGroups);
//        RData.put("toAll", toAll);
//        RData.put("fromUser", fromUser);
//        RData.put("messageSubject", messageSubject);
//        RData.put("messageCategory", messageCategory);
//        RData.put("messageContent", messageContent);
//        RData.put("messagePayload", messagePayload);
//
//        postData = RData.toString().getBytes();
//
//        if (appName == null || messageSubject == null || messageCategory == null || messageContent == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//        } else {
//            return pushConnect("messageSend", postData);
//        }
//    }
//
//    /**
//     * 푸시 메세지의 type list를 받아오는 메소드.<br>
//     */
//    private JSONObject getMessageTypeList(boolean agreeFlag, boolean deletedFlag, JSONArray messageTypeIds) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("agreeFlag", agreeFlag);
//        RData.put("deletedFlag", deletedFlag);
//        RData.put("messageTypeIds", messageTypeIds);
//
//        postData = RData.toString().getBytes();
//
//        if (messageTypeIds == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//        } else {
//            return pushConnect("getMessageTypeList", postData);
//        }
//
//    }
//
//    /**
//     * 푸시 수신 동의 정보를 받아오는 메소드를.<br>
//     */
//    private JSONObject getAgreementInfo(String userId, String appName, JSONArray messageTypeIds) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("userId", userId);
//        RData.put("appName", appName);
//        RData.put("messageTypeIds", messageTypeIds);
//
//        postData = RData.toString().getBytes();
//
//        if (userId == null || appName == null || messageTypeIds == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//        } else {
//            return pushConnect("getAgreementInfo", postData);
//        }
//    }
//
//    /**
//     * 푸시 수신 동의 정보를 업데이트 하는 메소드.<br>
//     */
//    private JSONObject updateAgreementInfo(String userId, String messageTypeId, boolean agreeFlag, String appName) throws JSONException {
//        byte[] postData = null;
//
//        JSONObject RData = new JSONObject();
//
//        RData.put("userId", userId);
//        RData.put("messageTypeId", messageTypeId);
//        RData.put("agreeFlag", agreeFlag);
//        RData.put("appName", appName);
//
//        postData = RData.toString().getBytes();
//
//        if (userId == null || appName == null || messageTypeId == null) {
//            JSONObject responseJSON = new JSONObject();
//
//            responseJSON.put("result", false);
//            responseJSON.put("resultCode", "9999");
//            responseJSON.put("resultMessage", "유효하지 않은 입력값 입니다.");
//
//            return responseJSON;
//        } else {
//            return pushConnect("updateAgreementInfo", postData);
//        }
//
//    }


    private void handleClickNotification() {
        LocalBroadcastManager.getInstance(cordova.getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter("notification-click"));
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);
            mCallbackContext.success();
        }
    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(cordova.getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    /**
     * 푸시 이벤트 별로 url을 조합해서 실제 서버로 요청을 보낸후 처리 결과를 반환해 주는 메소드.<br>
     */
    private void pushConnect(String type, byte[] postData) {
        int responseCode;
        URL url;

        String response = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;

        JSONObject responseJSON = new JSONObject();
        String strUrl;

        try {
            strUrl = PushUtil.getPushUrl(cordova.getActivity());

            int len = strUrl.length();
            int lastIndex = strUrl.lastIndexOf("/");

            if (len != lastIndex + 1) {
                strUrl += "/";
            }

            if (type.equals("regist")) {
                url = new URL(strUrl + "pushkeys");
            } else if (type.equals("check")) {
                url = new URL(strUrl + "messages/updateReceiptNotification");
            } else if (type.equals("getUnreadPushMessage")) {
                url = new URL(strUrl + "messages/getUnreadPushMessagesCount");
            } else if (type.equals("updateAlarmSetting")) {
                url = new URL(strUrl + "pushkeys/updateAlarmSetting");
            } else if (type.equals("markAsRead")) {
                url = new URL(strUrl + "messages/markAsRead");
            } else if (type.equals("alarmSettingInfo")) {
                url = new URL(strUrl + "pushkeys/getAlarmSetting");
            } else if (type.equals("messageSend")) {
                url = new URL(strUrl + "messages/fireMessages");
            } else if (type.equals("getMessageTypeList")) {
                url = new URL(strUrl + "messages/messageType/getMessageTypeList");
            } else if (type.equals("getAgreementInfo")) {
                url = new URL(strUrl + "messages/messageType/getAgreementInfo");
            } else if (type.equals("updateAgreementInfo")) {
                url = new URL(strUrl + "messages/messageType/updateAgreementInfo");
            } else {
                url = new URL(strUrl + "messages/getMessages");
            }

            Log.i("pushManager", url.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Content-Length", Integer.toString(postData.length));

            OutputStream out = conn.getOutputStream();
            out.write(postData);
            out.close();

            responseCode = conn.getResponseCode();
            Log.i("PushManager", "ResponseCode : " + responseCode);

            if (responseCode == 200) {
                is = conn.getInputStream();
                baos = new ByteArrayOutputStream();
                byte[] byteBuffer = new byte[1024];
                byte[] byteData = null;
                int nLength = 0;
                while ((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                    baos.write(byteBuffer, 0, nLength);
                }

                byteData = baos.toByteArray();
                response = new String(byteData);
                responseJSON = new JSONObject(response);
                resultHeader.put("result", true);
                resultBody = responseJSON;
                Log.i("PushManager", "response data : " + responseJSON.toString());
            } else {
                resultHeader.put("result", false);
                resultHeader.put("errorCode", "NE1" + responseCode);
                resultHeader.put("errorText", "http response error (" + responseCode + ")");

                Log.i("PushManager", "response data : " + responseJSON.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();

            try {
                resultHeader.put("result", false);
                resultHeader.put("errorCode", "NE0009");
                resultHeader.put("errorText", e.getClass().getName());
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

    }

    /**
     * Request Data를 key 와 value를 담은 DTO 클래스. <br>
     */
    private class KeyValueDataDTO {
        private String key;
        private String value;

        public KeyValueDataDTO() {
        }

        private KeyValueDataDTO(String key, String value) {
            this.key = key;
            this.value = value;
        }

        private String getKey() {
            return key;
        }

        private void setKey(String key) {
            this.key = key;
        }

        private String getValue() {
            return value;
        }

        private void setValue(String value) {
            this.value = value;
        }
    }

    /**
     * KeyValueDataDTO 를 담는 클래스. <br>
     */
    private class RDataDTO {
        private List<KeyValueDataDTO> keyValueData;

        private RDataDTO() {
            keyValueData = new ArrayList<KeyValueDataDTO>();
        }

        private List<KeyValueDataDTO> getKeyValueData() {
            return keyValueData;
        }

        private void setKeyValueData(List<KeyValueDataDTO> keyValueData) {
            this.keyValueData = keyValueData;
        }

        private void addKeyValueData(KeyValueDataDTO data) {
            keyValueData.add(data);
        }

        private void removeKeyValueData(KeyValueDataDTO data) {
            keyValueData.remove(data);
        }

        private void clearAllKeyValueData() {
            keyValueData.clear();
        }
    }

    private boolean isTablet(Context context) {
        boolean isTablet = (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
        String phoneModel = Build.MODEL;

        Log.d("GetDeviceInfo", "phoneModel = " + phoneModel);

        if (isTablet) {
            DisplayMetrics metrics = new DisplayMetrics();
            Activity activity = (Activity) context;
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            float yInches = metrics.heightPixels / metrics.ydpi;
            float xInches = metrics.widthPixels / metrics.xdpi;
            double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);

            if (diagonalInches >= 6.5) {
                if (phoneModel.equals("SM-T255S")) {
                    return false;
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private void sendCallback(JSONObject result) {
        try {
            if (result.getJSONObject(PARAM_HEADER).getBoolean("result")) {
                mCallbackContext.success(result);
            } else {
                mCallbackContext.error(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}