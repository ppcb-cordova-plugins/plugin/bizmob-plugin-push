var exec = require('cordova/exec');

// Event listener
exports._listener = {};

var BizMOBPush = {
    getPushKey:function(param, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "BizMOBPush", "GET_PUSHKEY", [param]);
    },

    pushRegistration:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "PUSH_REGISTRATION", [param]);
    },

    pushAlarmSettingInfo:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "PUSH_ALARM_SETTING_INFO", [param]);
    },

    pushUpdateAlarmSetting:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "PUSH_UPDATE_ALARM_SETTING", [param]);
    },

    pushGetMessages:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "PUSH_GET_MESSAGES", [param]);
    },

    pushMarkAsRead:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "PUSH_MARK_AS_READ", [param]);
    },

    pushGetUnreadMessageCount:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "PUSH_GET_UNREAD_PUSH_MESSAGE_COUNT", [param]);
    },

    setBadgeCount:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "SET_BADGE_COUNT", [param]);
    },

    sendMessages:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "SEND_PUSH_MESSAGE", [param]);
    },

    pushReceivedCheck:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "CHECK_PUSH_RECEIVED", [param]);
    },

    getPushMessageTypeList:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "GET_PUSH_MESSAGE_TYPE_LIST", [param]);
    },

    getPushAgreementInfo:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "GET_PUSH_AGREEMENT_INFO", [param]);
    },

    updatePushAgreementInfo:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "UPDATE_PUSH_AGREEMENT_INFO", [param]);
    },

    on:function(param, successCallback, errorCallback){
        exec(successCallback, errorCallback, "BizMOBPush", "ON_CLICK", [param]);
    }

};

module.exports = BizMOBPush;