# bizmob-plugin-push

<br></br>
* Push Plugin : 


<!-- ## 2. Screenshots

<!--<img src="screenshots/icon-actions-2.PNG" width="235"/>&nbsp;-->
<!--<img src="screenshots/icon-actions-4.PNG" width="235"/>&nbsp;-->
<!--<img src="screenshots/link-preview.PNG" width="235"/>&nbsp; -->


<br></br>
# Installation


* Plugin installation :
```
cordova plugin add git+https://gitlab.com/ppcb-cordova-plugins/plugin/bizmob-plugin-push.git --nofetch
```


* Add interface :
```
node_modules - @ionic-native 경로에 interface 를 추가한다. (아래 url에서 다운로드)
url : https://gitlab.com/ppcb-cordova-plugins/interface
[node_modules - @ionic-native - bizmob-plugin-push]
```


* Required file
```
google-services.json
noti_icon.png -> android res-drawable-xxdpi-noti_icon.png (size : 144 x 144)
```

<br></br>
# Usage 
* Get PushKey : 

```
param: object = {
    "body" : {
    },
    "header" : {
        "result" : false,
        "apiName" : "GET_PUSHKEY",
        "language" : "",
        "osType" : "",
        "displayType" : "",
        "errorCode" : "",
        "errorText" : ""
    }
}

BizMOBPush.getPushKey(param).then(
(res) => {
},
(err) => {
});

```

* Push Registration : 

```

param: object = {
    "body" : {
        "push_url" : "http://bizmobdev.mcnc.co.kr:5080/bizpush/",
        "push_key" : "",
        "user_id" : "userId",
        "app_name" : "bizmob4.0",
        "channel_id" : "id",
        "channel_name" : "name",
        "channel_description" : "description",
        "noti_importance" : "high" //default, high
    },
    "header" : {
        "result" : false,
        "apiName" : "PUSH_REGISTRATION",
        "language" : "",
        "osType" : "",
        "displayType" : "",
        "errorCode" : "",
        "errorText" : ""
    }
}

BizMOBPush.pushRegistration(param).then(
(res) => {
},
(err) => {
});


```

* Click Notification Callback :

````
param: object = {
    "body" : { 
        
    },
    "header" : {
        "result" : false,
        "apiName" : "ON_CLICK",
        "language" : "",
        "osType" : "",
        "displayType" : "",
        "errorCode" : "",
        "errorText" : ""
    }
}

BizMOBPush.on(param).then(
(res) => {
},
(err) => {
});

````

<br></br>
# Parameters 

- 플러그인에 사용되는 parameter 값의 타입, 설명


| Param | Type | Required | Description |
| :------ | :---- | :------- | :----------- |
| {} | Object | X | 특별한 param 값은 존재하지 않음 |
| success | Function | O | 플러그인 호출 성공시 Callback function. |
| failure | Function | O | 플러그인 호출 실패시 Callback function. |


<br></br>

# Changelog
 
* 1.0.0 플러그인배포 
 

<br></br>
# Credits

Created by [@Mobile CNC](https://mcnc.co.kr)
